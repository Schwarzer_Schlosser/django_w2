from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render

books = [{'id': 1,
          'title': 'Fluent Python',
          'released_year': 2015,
          'description': 'Python’s simplicity lets you become productive quickly, '
                         'but this often means you aren’t using everything it has to offer. '
                         'With this hands-on guide, you’ll learn how to write effective, '
                         'idiomatic Python code by leveraging its best—and possibly most neglected—features. '
                         'Author Luciano Ramalho takes you through Python’s core language features and libraries, '
                         'and shows you how to make your code shorter, faster, and more readable at the same time.',
          'author_id': 1},
         {'id': 2,
          'title': 'Python Cookbook',
          'released_year': 2013,
          'description': 'If you need help writing programs in Python 3, or want to update older Python 2 code, '
                         'this book is just the ticket. Packed with practical recipes written and tested with Python '
                         '3.3, this unique cookbook is for experienced Python programmers who want to focus on modern '
                         'tools and idioms.',
          'author_id': 2},
         {'id': 3,
          'title': 'Learning Python',
          'released_year': 2003,
          'description': 'Portable, powerful, and a breeze to use, Python is the popular open source object-oriented '
                         'programming language used for both standalone programs and scripting applications. '
                         'Python is considered easy to learn, but there\'s no quicker way to mastery of the language '
                         'than learning from an expert teacher. This edition of Learning Python puts you in the hands '
                         'of two expert teachers, Mark Lutz and David Ascher, whose friendly, well-structured prose '
                         'has guided many a programmer to proficiency with the language.',
          'author_id': 3},
         {'id': 4,
          'title': 'Programming Python',
          'released_year': 2010,
          'description': 'If you\'ve mastered Python\'s fundamentals, you\'re ready to start using it to get real work '
                         'done. Programming Python will show you how, with in-depth tutorials on the language\'s '
                         'primary application domains: system administration, GUIs, and the Web. You\'ll also explore '
                         'how Python is used in databases, networking, front-end scripting layers, text processing, '
                         'and more. This book focuses on commonly used tools and libraries to give you a comprehensive '
                         'understanding of Python’s many roles in practical, real-world programming.',
          'author_id': 3},
         {'id': 5,
          'title': 'Python vs. Go',
          'released_year': 2018,
          'description': 'Comparing Python with Go is a bit like comparing an SUV with a sports car: they were created '
                         'to serve different needs. Thanks to their simple syntax and careful design, you will '
                         'probably find Python and Go easier to learn and use than other mainstream languages that '
                         'you might have already studied. Given their gentle learning curve and phenomenal growth in '
                         'several fields, getting to know them is a sound investment now.',
          'author_id': 1}]

authors = [{'id': 1,
            'first_name': 'Luciano',
            'last_name': 'Ramalho',
            'age': 51},
           {'id': 2,
            'first_name': 'David',
            'last_name': 'Beazley',
            'age': 48},
           {'id': 3,
            'first_name': 'Mark',
            'last_name': 'Lutz',
            'age': 52}]


def list_books(request):
    for book in books:
        for author in authors:
            if book['author_id'] == author['id']:
                book.update(first_name=author['first_name'], last_name=author['last_name'])
    context = {'books': books, 'authors': authors, 'title': 'Books store'}
    return render(request, 'bookstore/books_list.html', context=context)


def book_detail(request, index):
    try:
        book = books[int(index) - 1]
        return render(request, 'bookstore/book_detail.html', context={'book': book, 'authors': authors})
    except:
        return HttpResponseNotFound(f'Not found {index = }')


def author_info(request, index):
    try:
        author = authors[int(index) - 1]
        return render(request, 'bookstore/author_info.html', context=author)
    except:
        return HttpResponseNotFound(f'Not found {index = }')


def author_books(request, ind):
    bk = []
    for i in books:
        if int(ind) == i['author_id']:
            bk.append(i['title'])
    if bk:
        return render(request, 'bookstore/author_books.html', {'bk': bk, 'title': 'Books'})
    else:
        return HttpResponseNotFound(f'Not found {ind = }')
